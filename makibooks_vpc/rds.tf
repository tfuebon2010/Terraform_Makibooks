################################################################################
# RDS Module
################################################################################

# Este código define dos recursos de Terraform para AWS: un grupo de subredes de base de datos y una instancia de base de datos.
# La instancia de base de datos utiliza el motor MySQL y se configura con una cantidad específica de almacenamiento asignado, una clase de instancia y otros parámetros.
# El grupo de subredes de la base de datos se configura con un nombre y una lista de subredes privadas.


resource "aws_db_instance" "makibooks" {
  allocated_storage       = 20
  engine                  = "mysql"
  engine_version          = "8.0.31"
  instance_class          = "db.t3.micro"
  identifier		  = "makibooksinstancia"
  db_name                 = "makibooks"
  username                = "gestor"
  password                = var.MASTER_PASSWORD
  vpc_security_group_ids  = [aws_security_group.rds_sg.id]
  db_subnet_group_name    = aws_db_subnet_group.makibooks.name
  storage_type            = "gp2"
  auto_minor_version_upgrade = false
  publicly_accessible     = false
  skip_final_snapshot	  = true
#  lifecycle {
#    prevent_destroy = true
#  }
}

resource "aws_db_subnet_group" "makibooks" {
  name       = "makibooks-subnet-group"
  subnet_ids = module.vpc.private_subnets
#  lifecycle {
#    prevent_destroy = true
#  }
}

