# Archivo de configuración de Terraform para AWS.
# Define un proveedor de AWS con una región especificada por una variable local.
# También recupera información sobre las zonas de disponibilidad en esa región y 
# las almacena en una variable local.
# Define algunas variables locales adicionales para el nombre, la región, 
# el CIDR de VPC y las etiquetas.


provider "aws" {
  region = local.region
}

data "aws_availability_zones" "available" {}

locals {
  name   = "${basename(path.cwd)}"
  region = "us-east-1"

  vpc_cidr = "10.0.0.0/16"
  azs      = slice(data.aws_availability_zones.available.names, 0, 3)

  tags = {
    ModuloAWS = "terraform-aws-vpc"
  }
}

