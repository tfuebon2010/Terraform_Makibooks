output "vpc_id" {
  description = "El ID de la VPC"
  value       = module.vpc.vpc_id
}

output "vpc_cidr_block" {
  description = "El bloque CIDR de la VPC"
  value       = module.vpc.vpc_cidr_block
}

output "vpc_enable_dns_support" {
  description = "Whether or not the VPC has DNS support"
  value       = module.vpc.vpc_enable_dns_support
}

output "vpc_enable_dns_hostnames" {
  description = "Whether or not the VPC has DNS hostname support"
  value       = module.vpc.vpc_enable_dns_hostnames
}

output "private_subnets" {
  description = "Lista de los IDs de las subredes privadas"
  value       = module.vpc.private_subnets
}

output "private_subnet_arns" {
  description = "List of ARNs of private subnets"
  value       = module.vpc.private_subnet_arns
}

output "private_subnets_cidr_blocks" {
  description = "Lista de los bloques CIDR de las subredes privadas"
  value       = module.vpc.private_subnets_cidr_blocks
}

output "public_subnets" {
  description = "Lista de los IDs de las subredes publicas"
  value       = module.vpc.public_subnets
}

output "public_subnets_cidr_blocks" {
  description = "Lista de los bloques CIDR de las subredes publicas"
  value       = module.vpc.public_subnets_cidr_blocks
}

output "public_route_table_ids" {
  description = "Lista los IDs de la tabla de rutas publicas"
  value       = module.vpc.public_route_table_ids
}

output "private_route_table_ids" {
  description = "Lista los IDs de la tabla de rutas privadas"
  value       = module.vpc.private_route_table_ids
}

output "public_internet_gateway_route_id" {
  description = "ID de la ruta al gateway de internet"
  value       = module.vpc.public_internet_gateway_route_id
}

output "nat_ids" {
  description = "ID de la IP elastica creada para el AWS NAT Gateway"
  value       = module.vpc.nat_ids
}

output "nat_public_ips" {
  description = "IPs elasticas creadas para el AWS NAT Gateway"
  value       = module.vpc.nat_public_ips
}

output "natgw_ids" {
  description = "ID del NAT Gateway"
  value       = module.vpc.natgw_ids
}

output "igw_id" {
  description = "ID del Internet Gateway"
  value       = module.vpc.igw_id
}

# VPC endpoints
output "vpc_endpoints" {
  description = "Array containing the full resource object and attributes for all endpoints created"
  value       = module.vpc_endpoints.endpoints
}
