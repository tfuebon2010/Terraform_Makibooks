# Módulo de Terraform para AWS

Este módulo de Terraform crea varios recursos en AWS, incluyendo una VPC, instancias EC2, un balanceador de carga y puntos finales de VPC.

## Uso

Para utilizar este módulo, asegúrate de tener Terraform instalado y configurado con tus credenciales de AWS. Luego, puedes utilizar el siguiente código como ejemplo para llamar al módulo en tu archivo de configuración de Terraform:

```hlc
module “aws_resources” { source = “<ruta al módulo>”

// Configuración del módulo }
```


Asegúrate de reemplazar `<ruta al módulo>` con la ruta real al módulo en tu sistema de archivos y de proporcionar la configuración necesaria para el módulo.

## Recursos creados

Este módulo crea los siguientes recursos en AWS:

- Una VPC con subredes públicas y privadas.
- Dos instancias EC2 en las subredes públicas.
- Un balanceador de carga de aplicación que enruta el tráfico a las instancias EC2.
- Un punto final de servicio RDS en la VPC.

## Configuración

Este módulo acepta varias variables de entrada para configurar los recursos creados. Consulta el archivo `variables.tf` en el directorio del módulo para obtener una lista completa de las variables disponibles y sus descripciones.


