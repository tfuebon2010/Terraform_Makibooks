################################################################################
# Security Groups Module
################################################################################

# Este código define varios recursos y bloques de datos de Terraform para AWS.
# Los bloques de datos `aws_subnet` recuperan información sobre dos subredes públicas específicas en una VPC.
# Estos bloques de datos se utilizan para obtener el bloque CIDR de cada subred.
#
# Luego, se definen tres grupos de seguridad.
# El primer grupo de seguridad, `rds_sg`, permite el acceso a una base de datos RDS en el puerto 3306 desde las dos subredes públicas especificadas anteriormente.
# También permite el acceso SSH desde cualquier dirección IP.
#
# El segundo grupo de seguridad, `web_sg`, permite el tráfico web en los puertos 80 y 443 desde cualquier dirección IP.
# También permite el acceso SSH desde cualquier dirección IP y permite todo el tráfico saliente.
#
# El tercer grupo de seguridad, `web_rds_sg`, permite todo el tráfico entre los dos primeros grupos de seguridad.

data "aws_subnet" "public_subnet_1" {
  id = module.vpc.public_subnets[0]
}

data "aws_subnet" "public_subnet_2" {
  id = module.vpc.public_subnets[1]
}

resource "aws_security_group" "rds_sg" {
  name        = "rds-sg"
  description = "Allow access to RDS database"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [data.aws_subnet.public_subnet_1.cidr_block]
  }

 ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [data.aws_subnet.public_subnet_2.cidr_block]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "web_sg" {
  name        = "web-sg"
  description = "Allow web traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "web_rds_sg" {
  name		= "web-rds-sg"
  vpc_id	= module.vpc.vpc_id


  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.web_sg.id, aws_security_group.rds_sg.id]
  }
}
