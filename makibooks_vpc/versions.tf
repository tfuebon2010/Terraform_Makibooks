# Este código define la versión requerida de Terraform y el proveedor de AWS que se utilizará en el archivo de configuración de Terraform.
# Específicamente, requiere una versión de Terraform igual o superior a 1.0 y una versión del proveedor de AWS igual o superior a 4.35.

terraform {
  required_version = ">= 1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.35"
    }
  }
}
