################################################################################
# VPC Module
################################################################################

# Este código define un módulo de Terraform para crear una VPC en AWS.
# El módulo se configura con un nombre, un bloque CIDR y una lista de zonas de disponibilidad.
# También se configura para crear subredes privadas y públicas utilizando funciones de Terraform como `slice` y `cidrsubnet`.
#
# Además, el módulo se configura con nombres específicos para las subredes privadas y públicas y se deshabilitan varias opciones de administración predeterminadas.
# También se habilitan las opciones de DNS y se configura para crear una puerta de enlace NAT.
#
# Finalmente, el módulo se configura con etiquetas específicas.

module "vpc" {
  source = "../"

  name = local.name
  cidr = local.vpc_cidr

  azs = slice(local.azs, 0, 2)
  private_subnets = [for k, v in slice(local.azs, 0, 2) : cidrsubnet(local.vpc_cidr, 8, k)]
  public_subnets = [for k, v in slice(local.azs, 0, 2) : cidrsubnet(local.vpc_cidr, 8, k + 4)]

  private_subnet_names = ["Subred privada uno", "Subred privada dos"]
  public_subnet_names = ["Subred publica uno", "Subred publica dos"]

  create_database_subnet_group  = false
  manage_default_network_acl    = false
  manage_default_route_table    = false
  manage_default_security_group = false

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_nat_gateway = true
  single_nat_gateway = true

  tags = local.tags
}

################################################################################
# VPC Endpoints Module
################################################################################

# Este código define dos módulos de Terraform para crear puntos finales de VPC en AWS.
# El primer módulo, `vpc_endpoints`, se configura con una ID de VPC y una lista de ID de grupo de seguridad.
# También se configura para crear un punto final de servicio RDS con opciones específicas como la lista de ID de subred y la lista de ID de grupo de seguridad.
#
# El segundo módulo, `vpc_endpoints_nocreate`, se configura para no crear ningún recurso.

module "vpc_endpoints" {
  source = "./modules/vpc-endpoints"

  vpc_id             = module.vpc.vpc_id
  security_group_ids = [aws_security_group.web_sg.id]

  endpoints = {
    rds = {
      service             = "rds"
      private_dns_enabled = false
      subnet_ids          = module.vpc.private_subnets
      security_group_ids  = [aws_security_group.rds_sg.id]
    }
  }

  tags = merge(local.tags, {
    Project  = "Secret"
    Endpoint = "true"
  })
}

module "vpc_endpoints_nocreate" {
  source = "./modules/vpc-endpoints"

  create = false
}
