################################################################################
# HAProxy Module
################################################################################
# Esta configuración crea una instancia de Amazon Web Services (AWS) y asocia una dirección IP elástica a ella. La instancia se crea utilizando la imagen de máquina de Amazon (AMI) especificada y el tipo de instancia `t2.micro`. Se coloca en una subred pública y se le asignan dos grupos de seguridad. También se le asigna una dirección IP pública y se le da un nombre de clave y etiquetas.

# El bloque `user_data` especifica un script que se ejecutará en la instancia cuando se inicie. Este script actualiza los paquetes del sistema operativo, instala HAProxy, crea un nuevo usuario llamado `ec2-user` y configura su acceso SSH. También configura HAProxy con una serie de opciones globales y predeterminadas, así como con frontends y backends para enrutar el tráfico HTTP y HTTPS.

# Finalmente, el recurso `aws_eip_association` asocia una dirección IP elástica existente con la instancia creada.

resource "aws_eip_association" "web_2" {
  instance_id   = aws_instance.web[1].id
  allocation_id = "eipalloc-001bd96889f7f359d"
}

resource "aws_instance" "haproxy" {
  ami           = "ami-01e5ff16fd6e8c542"
  instance_type = "t2.micro"
  subnet_id     = element(module.vpc.public_subnets, 0)
  vpc_security_group_ids = [aws_security_group.web_sg.id, aws_security_group.web_rds_sg.id]
  associate_public_ip_address = true
  key_name = "servidores"
  tags = {
    Name = "haproxy-instance"
  }

  user_data = <<-EOF
              #!/bin/bash
              sudo apt update
              sudo apt install -y haproxy
              sudo useradd -m -d /home/ec2-user -s /bin/bash -G admin,adm,dialout,cdrom,floppy,sudo,audio,dip,video,plugdev,netdev ec2-user
              sudo mkdir /home/ec2-user/.ssh
              sudo cp /home/admin/.ssh/authorized_keys /home/ec2-user/.ssh/authorized_keys
              sudo chmod 600 /home/ec2-user/.ssh/authorized_keys
              sudo chown -R ec2-user:ec2-user /home/ec2-user/.ssh
              echo 'ec2-user ALL=(ALL) NOPASSWD:ALL' | sudo tee /etc/sudoers.d/ec2-user
              echo "
              global
                log /dev/log local0
                log /dev/log local1 notice
                chroot /var/lib/haproxy
                stats timeout 30s
                user haproxy
                group haproxy
                daemon
                maxconn 2048
                tune.ssl.default-dh-param 2048

              defaults
                log global
                mode http
                option httplog
                option dontlognull
                timeout connect 5000
                timeout client 50000
                timeout server 50000
                option forwardfor
                option http-server-close


              frontend www-http
                bind *:80
                http-request add-header X-Forwarded-Proto http
                redirect scheme https if !{ ssl_fc }
                default_backend http_back

              frontend www-https
                bind *:443 ssl crt /etc/haproxy/certs/makibooks.tfuebon.es.pem
                http-request add-header X-Forwarded-Proto https
                acl letsencrypt-acl path_beg /.well-known/acme-challenge/
                use_backend letsencrypt-backend if letsencrypt-acl
                default_backend http_back

              backend http_back
                server web1 ${aws_instance.web[0].private_ip}:80 check
                server web2 ${aws_instance.web[1].private_ip}:80 check

              backend letsencrypt-backend
                server letsencrypt 127.0.0.1:9785

              " | sudo tee /etc/haproxy/haproxy.cfg

              EOF

}

resource "aws_eip_association" "eip_assoc" {
    instance_id   = aws_instance.haproxy.id
    allocation_id = "eipalloc-0322168cdee676f7f"
}

################################################################################
# Instances Module
################################################################################

# Este código define un recurso de Terraform para AWS que crea dos instancias EC2.
# El recurso utiliza la función `count` para crear dos instancias en lugar de una.
# Las instancias utilizan una AMI específica, que es una imagen de máquina de Amazon que contiene el sistema operativo y otros datos necesarios para iniciar una instancia.
#
# Las instancias también utilizan un tipo de instancia específico, `t2.micro`, que determina el hardware de la instancia.
# Cada instancia se configura para utilizar una subred pública específica de un módulo VPC y dos grupos de seguridad específicos.
#
# Además, las instancias se configuran para asociar una dirección IP pública, lo que les permite ser accesibles desde Internet.
# También se configuran para utilizar un par de claves específico, que se utiliza para acceder a las instancias mediante SSH.
#
# Finalmente, cada instancia se asigna una etiqueta de nombre específica que incluye su índice en el recuento.

resource "aws_instance" "web" {
  count         = 5
  ami           = "ami-0889a44b331db0194"
  instance_type = "t2.micro"
  subnet_id     = element(module.vpc.public_subnets, count.index)
  vpc_security_group_ids = [aws_security_group.web_sg.id, aws_security_group.web_rds_sg.id]
  associate_public_ip_address = true
  key_name = "servidores"
  tags = {
    Name = "server-web-${count.index + 1}"
  }
}

